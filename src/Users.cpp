#include "Users.hpp"
#include "SHA256.hpp"

namespace MediaTek {

    Users::Users() {}

    bool Users::loadUsers(Database __db) {
        using namespace std;
        using namespace nlohmann;

        this->db = __db;

        if (!this->db.isLoad()) {
            return false;
        }

        try
        {
            int nb_users = __db.getRawDatabase().at("users").size();

            for (int i = 0; i < nb_users; i++) {
                users_t user;
                user.id       = __db.getRawDatabase().at("users").at(i).at("id");
                user.type     = __db.getRawDatabase().at("users").at(i).at("type");
                user.login    = __db.getRawDatabase().at("users").at(i).at("user");
                user.password = __db.getRawDatabase().at("users").at(i).at("password");
                this->users.push_back(user);
            }
        }
        catch(const std::exception& e)
        {
            cout << "[ERROR] Unable to load users ..." << endl;
            return false;
        }
        

        return true;

    }

    bool Users::login(std::string __login, std::string __password) {
        using namespace std;
        using namespace nlohmann;

        for (auto user : this->users) {
            if (user.login.compare(__login) == 0) {
                if (user.password.compare(sha256(__password)) == 0) {
                    return true;
                }
            }
        }

        return false;
    }

    users_t Users::getUserByLogin(std::string __login) {
        using namespace std;
        using namespace nlohmann;

        for (auto user : this->users) {
            if (user.login.compare(__login) == 0)
                return user;
        }

        users_t u;
        return u;
    }

}