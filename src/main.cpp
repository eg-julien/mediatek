#include <iostream>
#include <string>

#include "Interface.hpp"
#include "nlohmann/json.hpp"

int main(int argc, char *agrv[]) {
    using namespace nlohmann;

    MediaTek::Database *DB = new MediaTek::Database();
    MediaTek::Users *USR = new MediaTek::Users();

    MediaTek::Interface *IF = new MediaTek::Interface();

    IF->setDB(*DB);
    IF->setUsers(*USR);

    IF->launch();

    DB->save("../assets/test.db");
    delete DB;
    delete USR;
    delete IF;
    
    return EXIT_SUCCESS;
}