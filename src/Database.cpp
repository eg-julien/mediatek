#include "Database.hpp"
#include "nlohmann/json.hpp"

namespace MediaTek {

    std::vector<std::string> type_name = { "book", "cd", "dvd", "article", "vhs", "journal", "test" };
    std::vector<std::string> category_name = { "science", "art", "sf", "romantic", "drama", "comedy", "thriller" };
    std::vector<std::string> key_name = { "author", "category", "description", "id", "isbn", "loan", "loan_at", "pages", "published", "subtitle", "title", "type", "website" };

    /**
     * @brief Construct a new Database:: Database object
     * 
     * @param __file 
     */
    Database::Database(const std::string __file) {
        this->file = __file;
        isLoaded = false;
        firstSearch = true;
    }

    /**
     * @brief Construct a new Database:: Database object
     * 
     */
    Database::Database() { isLoaded = false; }

    /**
     * @brief Permet de charger en mémoire la base de donnée contenu dans le fichier this->file.
     * 
     * @return true 
     * @return false 
     */
    bool Database::load(const std::string __file) {
        using namespace std;
        using namespace nlohmann;

        this->file = __file;

        ifstream file (__file);

        if (!file.is_open()) {
            return false;
        }

        file >> this->database;
        this->isLoaded = true;
        
        return true;
    }

    /**
     * @brief Permet de charger la base de donnée spécifiée directement lors de la création de l'objet.
     * 
     * @return true 
     * @return false 
     */
    bool Database::load(void) {
        using namespace std;
        using namespace nlohmann;

        ifstream file (this->file);

        if (!file.is_open()) {
            return false;
        }

        file >> this->database;
        isLoaded = true;
        
        return true;
    }

    /**
     * @brief Permet de sauvegarder l'état de la database dans le fichier __file.
     * 
     * @param __file std::string pointant le fichier où faire la sauvegarde
     * @return true 
     * @return false 
     */
    bool Database::save(const std::string __file) {
        using namespace std;

        if (!isLoaded) {
            cout << "\033[1;31m[ERROR] You must load a DB first with 'LOAD db_filename'." << endl;
            return false;
        }

        try
        {
            ofstream o(__file);
            o << setw(4) << this->database;
            o.close();
        }
        catch(const std::exception& e)
        {
            cout << "\033[1;31m[ERROR] Unable to save file at " << __file << " ..." << endl;
            return false;
        }

        return true;
    }

    bool Database::save(void) {
        using namespace std;

        if (!isLoaded) {
            cout << "\033[1;31m[ERROR] You must load a DB first with 'LOAD db_filename'." << endl;
            return false;
        }

        try
        {
            ofstream o(this->file);
            o << setw(4) << this->database;
            o.close();
        }
        catch(const std::exception& e)
        {
            cout << "\033[1;31m[ERROR] Unable to save file at " << this->file << " ..." << endl;
            return false;
        }

        return true;
    }

    /**
     * @brief Fonction qui permet de faire une recherche par categorie en passant en paramètre un id du tableau MediaTek::category.
     * 
     * @param __category
     * @return std::vector<int>
     */
    std::vector<int> Database::query_by_category(MediaTek::category_t __category) { 
        using namespace std;
        std::vector<int> ids;

        for (auto record : this->database.at("records")) {
            if (record.at("category") == category_name[__category]) {
                ids.push_back(record.at("id"));
            }
        }
        
        return ids;
    }

    /**
     * @brief Permet de faire une recherche __value par key dans la base de donnée. Les ids des données trouvées sont renvoyées dans un vector.
     * 
     * @param __key 
     * @param __value 
     * @return std::vector<int> 
     */
    std::vector<int> Database::query_by(key_t __key, const std::string __value) {
        using namespace std;
        vector<int> ids;

        for (auto record : this->database.at("records")) {
            if (record.at(key_name[__key]) == __value) {
                ids.push_back(record.at("id"));
            }
        }

        return ids;
    }

    /**
     * @brief Permet d'afficher la base de donnée contenu dans la mémoire courante du programme. Permet de faire du debug.
     * 
     */
    void Database::displayDatabase(void) {
        using namespace std;
        using namespace nlohmann;

        cout << "Displaying DB :" << endl;
        cout << setw(4) << this->database << endl;
    }

    /**
     * @brief Permet d'ajouter un élément à la base de donnée.
     * 
     * @param __element 
     * @return int 
     */
    int Database::insert(nlohmann::json __element) {

        if (!elementIsValid(__element)) {
            return false;
        }

        __element.emplace("id", this->database.at("records").size());
        this->database.at("records").push_back(__element);

        return true;
    }

    /**
     * @brief Permet de vérifier que l'élément fourni contient bien toute les clefs nécessaire à son stockage.
     * 
     * @param __element 
     * @return true 
     * @return false 
     */
    bool Database::elementIsValid(nlohmann::json __element) {
        bool t = __element.contains("author") && __element.contains("category") && __element.contains("description") && __element.contains("isbn") && __element.contains("loan") && __element.contains("loan_at") && __element.contains("pages") && __element.contains("published") && __element.contains("subtitle") && __element.contains("publisher") && __element.contains("title") && __element.contains("type") && __element.contains("website");

        if (t) {
            return true;
        }

        return false;
    }

    /**
     * @brief Permet de retourner la 'place' de l'élement dont l''id' est donné en paramètre
     * 
     * @param __id 
     * @return int 
     */
    int Database::placeOfId(int __id) {
        using namespace std;
        int r = 0;

        for (auto record : this->database.at("records")) {
            if (record.at(key_name[id]) == __id) {
                return r;
            }
            r++;
        }

        return -1;
    }

    /**
     * @brief Permet de supprimer tous les enregistrements de la base de donnée (uniquement les livres, dvd, cd, etc.)
     * 
     * @return true 
     * @return false 
     */
    bool Database::deleteAllRecords(void) {
        this->database.at("records").erase(0);
        this->save();
        return true;
    }

    /**
     * @brief Permet de supprimer un unique record à l'emplacement id
     * 
     * @param __id 
     * @return int 
     */
    int Database::deleteRecord(int __id) {
        if (placeOfId(__id) == -1) {
            return false;
        }
        this->database.at("records").erase(placeOfId(__id));
        return 0;
    }

    /**
     * @brief Permet de créer un tableau depuis un élément json de la base de donnée.
     * 
     * @param j 
     * @param record 
     */
    void from_json(const nlohmann::json& j, record_t& record) {
        record.id = j.at("id").get<int>();
        record.category = j.at("category").get<std::string>();
        record.description = j.at("description").get<std::string>();
        record.isbn = j.at("isbn").get<std::string>();
        record.author = j.at("author").get<std::string>();
        record.loan = j.at("loan").get<int>();
        record.loan_at = j.at("loan_at").get<std::string>();
        record.pages = j.at("pages").get<int>();
        record.published = j.at("published").get<std::string>();
        record.publisher = j.at("publisher").get<std::string>();
        record.subtitle = j.at("subtitle").get<std::string>();
        record.title = j.at("title").get<std::string>();
        record.type = j.at("type").get<std::string>();
        record.website = j.at("website").get<std::string>();
    }

    /**
     * @brief Permet de faire une recherche par id.
     * 
     * @param __id 
     * @return record_t 
     */
    record_t Database::query_by_id(int __id) {
        nlohmann::json j = this->database.at("records");
        std::string tmp;
        record_t r;

        for (auto record : j) {
            tmp = to_string(record.at("id"));
            if (tmp.compare(std::to_string(__id)) == 0) {
                from_json(record, r);
                return r;
            }
        }

        return r;
    }

    bool Database::loan(int __id) {
        if (placeOfId(__id) == -1)
            return false;
        
        std::time_t end_time = std::chrono::system_clock::to_time_t(std::chrono::system_clock::now());

        this->database["records"][__id]["loan"] = true;
        this->database["records"][__id]["loan_at"] = std::ctime(&end_time);

        save();

        return true;
    }

    bool Database::unloan(int __id) {
        if (placeOfId(__id) == -1)
            return false;
        
        std::time_t end_time = std::chrono::system_clock::to_time_t(std::chrono::system_clock::now());

        this->database["records"][__id]["loan"] = false;
        this->database["records"][__id]["loan_at"] = std::ctime(&end_time);

        save();

        return true;
    }
}