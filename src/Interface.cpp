#include "Interface.hpp"

#include <iostream>

namespace MediaTek {

    std::vector<std::string> cmd_n = { "BYE", "ADD", "LOAD", "SAVE", "SEARCH", "CLEAR", "LIST", "SHOW", "DELETE", "RESET", "HELP", "RELOAD", "LOAN", "UNLOAN" };

    Interface::Interface() {}

    std::string Interface::readKeyboard(void) {
        using namespace std;

        string s;
        getline(cin, s, '\n');
        return s;
    }

    void to_json(nlohmann::json &j, const record_t r) {
        j = nlohmann::json{
            { "author", r.author },
            {"category", r.category},
            {"description", r.description},
            {"isbn", r.isbn},
            {"loan", r.loan},
            {"loan_at", r.loan_at},
            {"pages", r.pages},
            {"published", r.published},
            {"publisher", r.publisher},
            {"subtitle", r.subtitle},
            {"title", r.title},
            {"type", r.type},
            {"website", r.website}
        };
    }

    bool Interface::execute(std::string __cmd) {
        using namespace std;  
        using namespace nlohmann;

        int pos = __cmd.find(" ");

        string key_cmd = __cmd.substr(0, pos);
        string key_arg = __cmd.substr(pos + 1, __cmd.size() - (pos + 1));

        cmd_t cmd = (cmd_t)-1;

        for (int i = 0; i < cmd_n.size(); i++) {
            if (key_cmd.compare(cmd_n[i]) == 0) {
                cmd = (cmd_t)i;
            }
        }

        if (cmd == (cmd_t)-1) {
            cout << "\033[1;31m[ERROR] - Command not found ... Try HELP to find some help." << endl;
            return false;
        }

        record_t r;
        string tmp;
        json j;

        switch (cmd)
        {
            case MediaTek::bye:
                if (this->db.isLoad()) {
                    cout << "\033[1;32m[INFO] Database will be saved to " << this->db.getFileName() << "\nGoodbye !" << endl;
                    this->db.save(this->db.getFileName());
                } else {
                    cout << "\033[1;32m[INFO] No database loaded. Goodbye !" << endl;
                }
                exit(0);
                break;
            
            case MediaTek::add:
                if (!this->db.isLoad())
                    break;
                
                if (!(this->connectedUser.type.compare("admin") == 0)) {
                    cout << "\033[1;31m[ERROR] You can't save the database as a client. Try login with administration credentials." << endl;
                    break;
                }

                if (key_arg.compare("ADD") == 0) {
                    cout << "\033[1;31m[ERROR] You should use ADD as follow : $ ADD type." << endl;
                    break;
                }

                cout << "\033[1;32m[INFO] You're about to add a " << key_arg << " in the database." << endl;

                r.type = key_arg;
                r.loan = 0;
                r.loan_at = "undefined";

                cout << "Title : ";
                getline(cin, tmp, '\n');
                r.title = tmp;

                cout << "Subtitle : ";
                getline(cin, tmp, '\n');
                r.subtitle = tmp;

                cout << "Author : ";
                getline(cin, tmp, '\n');
                r.author = tmp;

                cout << "Category : ";
                getline(cin, tmp, '\n');
                r.category = tmp;

                cout << "Description : ";
                getline(cin, tmp, '\n');
                r.description = tmp;

                cout << "ISBN : ";
                getline(cin, tmp, '\n');
                r.isbn = tmp;

                cout << "Pages : ";
                getline(cin, tmp, '\n');
                r.pages = stoi(tmp);

                cout << "Publish date : (format : 2021-12-16T00:00:00.000Z)";
                getline(cin, tmp, '\n');
                r.published = tmp;

                cout << "Publisher : ";
                getline(cin, tmp, '\n');
                r.publisher = tmp;

                cout << "Website : ";
                getline(cin, tmp, '\n');
                r.website = tmp;

                to_json(j, r);

                if (this->db.insert(j)) {
                    cout << "\033[1;32m[INFO] " << r.title << " added to database successfully." << endl;
                } else {
                    cout << "\033[1;31m[ERROR] " << r.title << " cannot be save ..." << endl;
                }


                break;
            case MediaTek::load: // modifier le système pour si les utilisateurs ou quoi sont déjà chargés
                if (this->db.load(key_arg)) {
                    cout << "\033[1;32m[INFO] Database loaded successfully." << endl;
                    if (this->users.loadUsers(this->db)) {
                        cout << "\033[1;31mUser account loaded. You may now logging to access admin tools. Client's login is 'client' with no password." << endl;
                        cout << "Login : ";
                        string __login;
                        string __pass;
                        getline(cin, __login, '\n');

                        if (!(__login.compare("client") == 0)) {
                            cout << "Password : ";
                            getline(cin, __pass, '\n');

                            if (this->users.login(__login, __pass)) {
                                this->connectedUser = this->users.getUserByLogin(__login);
                                cout << "\033[1;32mConnected ! \nWelcome " << this->connectedUser.login << ",\nType HELP to display help." << endl; 
                            } else {
                                cout << "\033[1;31m[ERROR] Unable to authenticate you. Please retry ..." << endl;
                                break;
                            }
                        } else {
                            this->connectedUser.id = -1;
                            this->connectedUser.login = "client";
                            this->connectedUser.password = "none";
                            cout << "\033[1;32mConnected : \nWelcom to the client side.\nType HELP to display help." << endl;
                        }
                    }
                } else {
                    cout << "\033[1;31m[ERROR] Unable to open database at " << key_arg << endl;
                }
                break;
            case MediaTek::save:

                if (!this->db.isLoad())
                    break;
                
                if (!(this->connectedUser.type.compare("admin") == 0)) {
                    cout << "\033[1;31m[ERROR] You can't save the database as a client. Try login with admin credentials." << endl;
                    break;
                }

                bool err;
                if (key_arg.compare("SAVE") == 0) {
                    err = this->db.save();
                    cout << "\033[1;32m[INFO] Saving database at " << this->db.getFileName() << endl;
                } else {
                    err = this->db.save(key_arg);
                    cout << "\033[1;32m[INFO] Saving database at " << key_arg << endl;
                }

                if (err) {
                    cout << "\033[1;32m[INFO] Database saved !" << endl;
                } else {
                    cout << "\033[1;31m[ERROR] Unable to save database ..." << endl;
                }
                break;
            
            case MediaTek::list:
                if (this->db.isLoad()) {
                    json records = this->db.getRawDatabase().at("records");
                    for (auto r : records) {
                        cout << "ID : " << r.at("id") 
                            << "\t Title : " << r.at("title")
                            << "\t Author : " << r.at("author") << endl;
                    }
                } else {
                    cout << "\033[1;31m[ERROR] Database must be load before hands." << endl;
                }

                break;

            case MediaTek::show:
                if (this->db.isLoad()) {
                    record_t r = this->db.query_by_id(stoi(key_arg));
                    if (r.id != stoi(key_arg)) {
                        cout << "\033m[1;31[ERROR] This record doesn't exist." << endl;
                    } else {
                        cout << "Informations about record ID no " << r.id << endl;
                        cout << "Title : " << r.title << ",\tSubtitle : " << r.subtitle
                        << "\nDescription : " << r.description << "\n Type : " << r.type
                        << " Duration (pages for books and mn for CD) : " << r.pages
                        << "\n ISBN : " << r.isbn << ",\tPublisher : " << r.publisher << "\tat : " << r.published
                        << "\n Loan : " << r.loan << ",\tLoan at : " << r.loan_at
                        << "\n Category : " << r.category << ",\tWebsite : " << r.website << endl;
                    }
                } else {
                    cout << "\033[1;31m[ERROR] Database must be load before hands." << endl;
                }
                break;

            case MediaTek::reload:
                if (this->db.load()) {
                    cout << "\033[1;32m[INFO] Database has been updated." << endl;
                } else {
                    cout << "\033[1;31m[ERROR] Unable to reload database" << endl;
                }
                break;
            
            case MediaTek::del:
                if (!(this->connectedUser.type.compare("admin") == 0)) {
                    cout << "\033[1;31m[ERROR] You can't delete a record as a client. Try login with admin credentials." << endl;
                    break;
                }

                if (key_arg.compare("DELETE") == 0) {
                    cout << "\033[1;31m[ERROR] You should use DELETE as follow : $ DELETE id." << endl;
                    break;
                }

                if (this->db.isLoad()) {
                    cout << "\033[1;32mAre you sure ? Y/n:";
                    string conf;
                    getline(cin, conf, '\n');
                    if (conf.compare("Y") == 0) {
                        this->db.deleteRecord(stoi(key_arg));
                        cout << "\033[1;32m[INFO] Record n°" << key_arg << " has been deleted." << endl;
                    } else {
                        cout << "\033[1;31mConfirmation cancelled." << endl;
                    }
                    
                } else {
                    cout << "\033[1;31[ERROR] Database must be load before hands." << endl;
                }
                break;

            case MediaTek::reset:
                if (!(this->connectedUser.type.compare("admin") == 0)) {
                    cout << "\033[1;31m[ERROR] You can't reset records as a client. Try login with admin credentials." << endl;
                    break;
                }

                if (this->db.isLoad()) {
                    cout << "\033[1;31mAre you sure ? Y/n: ";
                    string conf;
                    getline(cin, conf, '\n');
                    if (conf.compare("Y") == 0) {
                        this->db.deleteAllRecords();
                        cout << "\033[1;32m[INFO] All records in the DB were deleted." << endl;
                    } else {
                        cout << "\033[1;31m[ERROR] Confirmation cancelled." << endl;
                    }
                } else {
                    cout << "\033[1;31m[ERROR] - No database loaded." << endl;
                }

                break;
            case MediaTek::help:
                if (!(this->connectedUser.type.compare("admin") == 0)) {
                    cout << "\033[1;32m" << "[HELP] - All commands available :"
                    << "\nBYE - Quit the application and save current opened database.\n"
                    //<< "ADD type - Launch sub-application to add a record into the database.\n"
                    << "LOAD filename - Load the database saved at filename\n"
                    << "SAVE (filename) - With a filename specified, save current database to filename. If no filename specified save current database.\n"
                    << "SEARCH query - Launch the search sub-aplication.\n"
                    << "CLEAR - Exit SEARCH sub-application.\n"
                    << "LIST - List all records available in the database.\n"
                    << "SHOW id - Gives details about a specific record id.\n"
                    << "LOAN id - Let you loan a record for 2 weeks.\n"
                    << "UNLOAN id - Set the record as available for loan."
                    //<< "DELETE id - Delete a record with id 'id' from the current database. If you don't SAVE the database this has no effect.\n"
                    //<< "RESET - Delete all records in the database."
                    << endl;
                } else {
                    cout << "\033[1;32m" << "[HELP] - All commands available :"
                    << "\nBYE - Quit the application and save current opened database.\n"
                    << "ADD type - Launch sub-application to add a record into the database.\n"
                    << "LOAD filename - Load the database saved at filename\n"
                    << "SAVE (filename) - With a filename specified, save current database to filename. If no filename specified save current database.\n"
                    << "SEARCH - Launch search sub-application\n"
                    << "CLEAR - Exit SEARCH sub-application.\n"
                    << "LIST - List all records available in the database.\n"
                    << "SHOW id - Gives details about a specific record id.\n"
                    << "LOAN id - Let you loan a record for 2 weeks.\n"
                    << "UNLOAN id - Set the record as available for loan.\n"
                    << "DELETE id - Delete a record with id 'id' from the current database. If you don't SAVE the database this has no effect.\n"
                    << "RESET - Delete all records in the database."
                    << endl;
                }
            case MediaTek::search:
                if (key_arg.compare("SEARCH") == 0) {
                    cout << "\033[1;31m[ERROR] Tou should use search as follow : $ SEARCH query." << endl;
                    break;
                }

                if (this->db.isLoad()) {
                    auto db_json = this->db.getRawDatabase().at("records");

                    if ((this->db.getFirstSearch())) {
                        db_json = this->db.getSearchDatabase();
                        cout << "\033[1;33m[INFO] Your search is made in the previous search results. If you want a global search please use CLEAR." << endl;
                    } else {
                        this->db.setFirstSearch(false);
                        cout << "\033[1;33m[INFO] Your search is made in the global database." << endl;
                    }
                    
                    for (auto r : db_json) {
                        if (r["title"] == key_arg || r["subtitle"] == key_arg || r["id"] == key_arg || r["category"] == key_arg || r["type"] == key_arg) {
                            cout << "Result, ID n°" << r["id"] << "\tTitle :" << r["title"] << endl;
                            this->db.getSearchDatabase().push_back(r);
                        }
                    }
                } else {
                    cout << "\033[1;31m[ERROR] - No database loaded." << endl;
                }

                break;
            
            case MediaTek::clear:
                try
                {
                    this->db.getSearchDatabase().erase(0);
                    this->db.setFirstSearch(true);
                }
                catch(const std::exception& e) { cout << "\033[1;33m[INFO] The search result db was already empty." << endl; }
                cout << "\033[1;33m[INFO] The search result db is now empty." << endl;
                break;
            default:
                break;
            case MediaTek::loan_i:
                if (key_arg.compare("LOAN") == 0) {
                    cout << "\033[1;31m[ERROR] You should use LOAN as follow : $ LOAN id." << endl;
                    break;
                }

                if (this->db.isLoad()) {
                    if (this->db.loan(stoi(key_arg)) == true) {
                        cout << "\033[1;32m[INFO] You just loan stuff with id : " << key_arg << " for two weeks." << endl;
                    }
                } else {
                    cout << "\033[1;31m[ERROR] - No database loaded." << endl;
                }
                break;
            case MediaTek::unloan_i:
                if (key_arg.compare("UNLOAN") == 0) {
                    cout << "\033[1;31m[ERROR] You should use UNLOAN as follow : $ UNLOAN id." << endl;
                    break;
                }

                if (this->db.isLoad()) {
                    if (this->db.unloan(stoi(key_arg)) == true) {
                        cout << "\033[1;32m[INFO] You just unloan stuff with id : " << key_arg << "." << endl;
                    }
                } else {
                    cout << "\033[1;31m[ERROR] - No database loaded." << endl;
                }
                break;
        }

        return true;
    }

    void displayStatus() {
        using namespace std;
        


    }

    void Interface::launch(void) {
        using namespace std;

        cout << "Welcome to MediaTek's App !" << endl;
        // Check if db has users -> if yes then ask for authentication -> if no launch()
        
        while (true) {
            if (!this->db.isLoad()) {
                cout << "\033[1;31m[INFO] No database loaded yet. Please load one with 'LOAD db_filename'." << endl;
                cout << "$ ";
                this->execute(this->readKeyboard());
            } else {
                cout << "\033[0m$ ";
                this->execute(this->readKeyboard());
            }
        }

    }

}