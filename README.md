# MediaTek

La médiathèque spécialisée dans la Tek(no).

## How to use

Clone this repository with : 
`git clone https://gitlab.com/eg-julien/mediatek.git`

Now you have to compile sources to create an executable :
```bash
$ cd mediatek/
$ mkdir build/
$ cd build/
$ cmake ..
$ make
$ ./MediaTek
```

## In app commands :

Since MediaTek app is started, we now have to load a database (default database are stored in /assets/).
`$ LOAD ../assets/test.db`

If credentials are stored in the users array, you might have to login (admin:toto or client:).

Type `$ HELP` in the app to get full help.

## TODO :

- ~~Add user system~~
- ~~Add loan system~~
- Modify search system to create an algorithm for research in the DB (like google)
- ~~Database saved in json format~~
- Use an online stored database
- Rajouter une commande permettant d'ajouter de nouveaux utilisateurs / admins
- Rajouter une commande permettant d'afficher un planning sur la disponibilité d'un enregistrement
- Système de réservation
