#ifndef SEARCH_H
#define SEARCH_H

#include "Database.hpp"

namespace MediaTek {

    class Search : public Database
    {
        private:
            std::vector<record_t> results;
        public:
            launch(const std::string query);
    }

}

#endif