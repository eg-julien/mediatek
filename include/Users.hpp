#ifndef USERS_H
#define USERS_H

#include "Database.hpp"

#include <vector>
#include <iostream>
#include <string>

namespace MediaTek {

    struct users_t
    {
        int id;
        std::string type; // 0 = admin 1 = client
        std::string login;
        std::string password;
    };
    

    class Users {
        private:
            Database db;
            std::vector<users_t> users;
            bool isAuth = false;
            users_t user;

        public:
            Users();
            bool loadUsers(Database __db);
            bool login(std::string __login, std::string __password);
            users_t getUserByLogin(std::string __login);
    };

}

#endif