#ifndef DATABASE_H
#define DATABASE_H

#define DEBUG 1

#include <iostream>
#include <string>
#include <format>
#include <chrono>
#include <ctime>
#include <fstream>
#include <sstream>
#include <vector>
#include <algorithm>

#include "../include/nlohmann/json.hpp"

namespace MediaTek {

    enum type_t { book, cd, dvd, article, vhs, journal, test };
    enum category_t { science, art, sf, romantic, drama, comedy, thriller };
    enum key_t { author, category, description, id, isbn, loan, loan_at, pages, published, subtitle, title, type, website };

    struct record_t {
        int id;
        std::string author;
        std::string category;
        std::string description;
        std::string isbn;
        bool loan;
        std::string loan_at;
        int pages;
        std::string published;
        std::string publisher;
        std::string subtitle;
        std::string title;
        std::string type;
        std::string website;
    };

    class Database
    {
        private:
            std::string file;
            nlohmann::json database;
            nlohmann::json searchDatabase;
            bool isLoaded;
            bool firstSearch;

            bool elementIsValid(nlohmann::json __element);
        
        public:
            Database(const std::string __file);
            Database();
            bool load(const std::string __file); // Permet de charger la DB depuis le fichier file
            bool load(void);
            bool save(const std::string __file); // Permet de sauvegarder la DB dans un fichier __file
            bool save(void);

            std::string getFileName() { return this->file; }
            nlohmann::json getRawDatabase() { return this->database; }
            bool isLoad() { return this->isLoaded; }

            int placeOfId(int __id);

            int insert(nlohmann::json __element);
            std::vector<int> query_by_category(category_t __category);
            std::vector<int> query_by(key_t __key, const std::string __value);
            record_t query_by_id(int __id);
            int deleteRecord(int __id);
            bool deleteAllRecords(void);
            
            bool loan(int __id);
            bool unloan(int __id);
            void displayDatabase(void);

            bool getFirstSearch() { return firstSearch; }
            bool setFirstSearch(bool __value) { firstSearch = __value; return __value; }
            nlohmann::json getSearchDatabase() { return this->searchDatabase; }
    };
}

#endif