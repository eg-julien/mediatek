#ifndef INTERFACE_H
#define INTERFACE_H

#include "Database.hpp"
#include "Users.hpp"
#include <vector>
#include <iostream>

namespace MediaTek {

    enum cmd_t { bye, add, load, save, search, clear, list, show, del, reset, help, reload, loan_i, unloan_i };

    class Interface {
        private:
            std::string readKeyboard(void);
            bool execute(std::string __cmd);
            void displayStatus(void);

            Database db;
            Users users;

            bool isConnected;
            users_t connectedUser;
        public:
            Interface();
            void launch();
            void setDB(Database __db) { this->db = __db; }
            void setUsers(Users __users) { this->users = __users; }
    };

}

#endif